var themeChange = document.getElementById('change-theme');
var light = document.getElementById('label_light')
var dark = document.getElementById('label_dark')

themeChange.onclick = function(){
    if(themeChange.checked == true){
        document.body.classList.add("active")
        light.classList.add('d-none');
        dark.classList.add('d-block');
       
    }else{
        document.body.classList.remove("active")
        light.classList.remove('d-none');
        dark.classList.remove('d-block')
    }
   
}